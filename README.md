# WES-CLI

## Installation

### Windows (64-bit)

1) Clone the code.

2) Copy `./build/config.json` and `./build/wes-cli.exe` somewhere your `PATH` looks.

3) Edit the `config.json` file you copied so it matches your database configuration.

4) From terminal, bash, command prompt, powershell, or whatever you use run `wes-cli [command]`.

### Other architectures

1) Edit the `dotnet publish` line in  `/build/publish.sh` so the architecture matches your target [RID](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog) instead of `win-x64`. 

2) Follow the [instructions to install Warp](https://github.com/dgiagio/warp#quickstart-with-net-core) and make sure the binary is added to your `PATH`.

3) Run `sudo ./publish.sh` to generate a new binary. It needs admin permissions otherwise .NET won't be able to copy the necessary files when publishing.

4) Follow the instructions for Windows (64-bit) but copy your newly generated binary instead of `wes-cli.exe`.

## Commands
**All command arguments are required**

**All commands should be prepended by whatever you saved the binary as in your PATH or /usr/local/bin:**

`wes-cli empty`

**Add the `-d` flag to the beginning of the command to see stacktraces to caught exceptions. Additionally, the `-d` flag will log more info when importing via spreadsheet:**

`wes-cli -d import my-entities.xlsx`

|Command|Description|Usage|
|-------|-----------|-----|
|`container`|Add single container|`container add [TemplatePK] [ContainerType] [Barcode] [LocationPK] [Status] [Orientation] [PresentedSide]`|
|`containertemplate`|Add single container template|`containertemplate add [TemplateName] [TemplateDescription] [Height] [Length] [Width] [MaximumWeight] [IsGoodsToPerson]`|
|`empty`|Empties the database|`empty`|
|`help`|For now this just opens the browser to this readme hosted on bitbucket|`help`|
|`import`|Imports data from spreadsheet*|`import [FilePath]`|
|`inventory`|Add single inventory item|`inventory add [UnitOfMeasurePK] [ContainerPK] [ContainerSide] [ContainerX] [ContainerY] [Quantity]`|
|`location`|Add single location|`location add [LocationName] [Barcode] [LocationType] [IsEnabled]`|
|`order`|Add single order|`order add [ExternalOrderNumber] [ContainerType] [Priority] [IsSingle]`|
|`orderline`|Add single order line|`orderline add [OrderPK] [ExternalOrderLineNumber] [UnitOfMeasurePK] [RequiredQuantity]`|
|`product`|Add single product|`product add [Name]`|
|`reset`|Runs `empty` and reseeds database with original seed script|`reset`|

\* Use `./build/import-template.xlsx` as your template for spreadsheet imports.

## Generators

You can also generate some entities and let the software take care of the fields for you.

|Command|Usage|
|-|-|
|`container`|`container generate [NumberOfContainers]`|
|`inventory`*|`inventory generate [NumberOfInventoryItems]`|
|`order`|`order generate [NumberOfOrders] [MinOrderLines] [MaxOrderLines]`|
|`product`|`product generate [NumberOfProducts]`|

\* Inventory generation doesn't take into account the natural key so it's possible you will get an exception when generating large numbers of unique inventory items. Or possibly just randomly when generating small number of inventory items.