using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task Order(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddOrderAsync(verbArgs);
                    return;
                case nameof(VerbEnum.generate):
                    await GenerateOrdersAsync(verbArgs);
                    return;
            }
        }

        internal async Task GenerateOrdersAsync(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Usage: order generate [NumberOfOrders] [MinOrderLines] [MaxOrderLines]");
                return;
            }

            var numberOfOrders = int.Parse(args[0]);
            var minOrderLines = int.Parse(args[1]);
            var maxOrderLines = int.Parse(args[2]) + 1; // add 1 since Random.Next(x, y) gives random from [x, y)

            var orders = new List<FulfillmentOrders>();
            for (int i = 0; i < numberOfOrders; i++)
            {
                var numberOfOrderLines = random.Next(minOrderLines, maxOrderLines);
                var orderLines = new List<FulfillmentOrderLines>();

                for (int j = 0; j < numberOfOrderLines; j++)
                {
                    var productPK = random.Next(1, context.Products.Count());
                    var product = await context.Products.Include(p => p.UnitsOfMeasure).SingleAsync(p => p.PrimaryKey == productPK); // random product
                    var unitOfMeasurePK = product.UnitsOfMeasure.OrderBy(o => Guid.NewGuid()).ToList().First().PrimaryKey; // random unit of measure
                    var quantity = random.Next(1, 10); // TODO: make this not just hardcoded to 1-10
                    orderLines.Add(new FulfillmentOrderLines()
                    {
                        UnitOfMeasurePrimaryKey = unitOfMeasurePK,
                        RequiredQuantity = quantity,
                        ExternalOrderLineNumber = (j + 1).ToString(),
                        AllocatedQuantity = 0,
                        FulfilledQuantity = 0,
                        ShortedQuantity = 0,
                        Characteristics = "",
                        CreatedBy = CLI_CLIENT_NAME,
                        CreatedDate = DateTime.UtcNow,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = CLI_CLIENT_NAME
                    });
                }

                var containerType = context.Containers.OrderBy(o => Guid.NewGuid()).First().ContainerType; // random container

                orders.Add(new FulfillmentOrders()
                {
                    FulfillmentOrderLines = orderLines,
                    IsSingle = orderLines.Count == 1,
                    Priority = 3,
                    ExternalOrderNumber = Guid.NewGuid().ToString(),
                    ContainerType = containerType,
                    Status = "New",
                    GroupName = "", // can be null, but seed file used ""
                    CutOffDate = DateTime.Parse("2009-05-16"), // date used in original sql seed file
                    CreatedBy = CLI_CLIENT_NAME,
                    CreatedDate = DateTime.UtcNow,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = CLI_CLIENT_NAME
                });
            }

            await context.FulfillmentOrders.AddRangeAsync(orders);
        }

        internal async Task AddOrderAsync(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: order add [ExternalOrderNumber] [ContainerType] [Priority] [IsSingle]");
                return;
            }

            await context.FulfillmentOrders.AddAsync(new FulfillmentOrders()
            {
                ExternalOrderNumber = args[0],
                ContainerType = args[1],
                Priority = int.Parse(args[2]),
                IsSingle = args[3] == "1",
                Status = "New",
                GroupName = "", // can be null, but seed file used ""
                CutOffDate = DateTime.Parse("2009-05-16"), // date used in original sql seed file
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            });
        }
    }
}
