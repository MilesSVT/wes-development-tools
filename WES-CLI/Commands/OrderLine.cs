using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task OrderLine(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddOrderLineAsync(verbArgs);
                    return;
            }
        }

        internal async Task AddOrderLineAsync(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: orderline add [OrderPK] [ExternalOrderLineNumber] [UnitOfMeasurePK] [RequiredQuantity]");
                return;
            }

            await context.FulfillmentOrderLines.AddAsync(new FulfillmentOrderLines()
            {
                FulfillmentOrderPrimaryKey = int.Parse(args[0]),
                ExternalOrderLineNumber = args[1],
                UnitOfMeasurePrimaryKey = int.Parse(args[2]),
                RequiredQuantity = int.Parse(args[3]),
                AllocatedQuantity = 0,
                FulfilledQuantity = 0,
                ShortedQuantity = 0,
                Characteristics = "",
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            });
        }
    }
}
