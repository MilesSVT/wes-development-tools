using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task Inventory(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddInventoryAsync(verbArgs);
                    return;
                case nameof(VerbEnum.generate):
                    await GenerateInventoryAsync(verbArgs);
                    return;
            }
        }

        internal async Task GenerateInventoryAsync(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: inventory generate [NumberOfInventoryItems]");
                return;
            }

            var numberOfInventoryItems = int.Parse(args[0]);
            var inventory = new List<Inventory>();
            for (int i = 0; i < numberOfInventoryItems; i++)
            {
                var unitOfMeasurePK = context.UnitsOfMeasure.OrderBy(o => Guid.NewGuid()).ToList().First().PrimaryKey; // random unit of measure
                var containerPK = context.Containers.OrderBy(o => Guid.NewGuid()).ToList().First().PrimaryKey; // random container
                inventory.Add(new Inventory()
                {
                    UnitOfMeasurePrimaryKey = unitOfMeasurePK,
                    ContainerPrimaryKey = containerPK,
                    ContainerSide = random.Next(1, 3), // seed data was either 1 or 2
                    ContainerX = random.Next(1, 3), // seed data was either 1 or 2
                    ContainerY = random.Next(1, 6), // seed data was [1, 5]
                    Quantity = random.Next(1, 31), // seed data was [1, 30]
                    InventoryCharacteristics = "",
                    Status = "Good",
                    CreatedBy = CLI_CLIENT_NAME,
                    CreatedDate = DateTime.UtcNow,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = CLI_CLIENT_NAME
                });
            }

            await context.Inventory.AddRangeAsync(inventory);
        }

        internal async Task AddInventoryAsync(string[] args)
        {
            if (args.Length != 6)
            {
                Console.WriteLine("Usage: inventory add [UnitOfMeasurePK] [ContainerPK] [ContainerSide] [ContainerX] [ContainerY] [Quantity]");
                return;
            }

            await context.Inventory.AddAsync(new Inventory()
            {
                UnitOfMeasurePrimaryKey = int.Parse(args[0]),
                ContainerPrimaryKey = int.Parse(args[1]),
                ContainerSide = int.Parse(args[2]),
                ContainerX = int.Parse(args[3]),
                ContainerY = int.Parse(args[4]),
                Quantity = int.Parse(args[5]),
                InventoryCharacteristics = "",
                Status = "Good",
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            });
        }
    }
}
