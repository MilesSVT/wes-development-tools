using System.Threading.Tasks;
using System;

namespace WESCLI
{
    public partial class Commands
    {
        // needs to be async for the CLIRuntime commandMap to work
#pragma warning disable CS1998
        public async Task Help(string[] args) => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
        {
            UseShellExecute = true,
            FileName = "https://bitbucket.org/MilesSVT/wes-development-tools/src/master/README.md"
        });
#pragma warning restore CS1998
    }
}
