using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using OfficeOpenXml;
using System.Collections.Generic;

// TODO: optional logging

namespace WESCLI
{
    public partial class Commands
    {
        internal class Excel
        {
            // TODO: put this in some config somewhere
            private const string host = "localhost";
            private const string database = "SVT_MVP";
            private const string login = "sa";
            private const string password = "Hithere312";
            private readonly string connectionString = $"Server={host};Database={database};User Id={login};Password={password};";

            private readonly Commands _commands;

            public Excel(Commands commands)
            {
                _commands = commands;
            }
            public async Task Import(string[] args)
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Usage: import [Path]");
                    return;
                }

                var path = args[0];
                var file = new FileInfo(path);
                using (ExcelPackage p = new ExcelPackage(file))
                {
                    // first wave
                    // concurrency
                    var locationsTask = ImportLocations(p.Workbook.Worksheets["Locations"]);
                    var productsTask = ImportProducts(p.Workbook.Worksheets["Products"]);
                    var locationZonesTask = ImportLocationZones(p.Workbook.Worksheets["LocationZones"]);
                    var containerTemplatesTask = ImportContainerTemplates(p.Workbook.Worksheets["ContainerTemplates"]);
                    // results
                    var locations = await locationsTask;
                    var locationZones = await locationZonesTask;
                    var products = await productsTask;
                    var containerTemplates = await containerTemplatesTask;

                    // second wave
                    // concurrency
                    var locationZoneMappingsTask = ImportLocationZoneMappings(p.Workbook.Worksheets["LocationZoneMappings"], locations, locationZones);
                    var unitsOfMeasureTask = ImportUnitsOfMeasure(p.Workbook.Worksheets["UnitsOfMeasure"], products);
                    var containerTemplateCompartmentsTask = ImportContainerTemplateCompartments(p.Workbook.Worksheets["ContainerTemplateCompartments"], containerTemplates);
                    var containersTask = ImportContainers(p.Workbook.Worksheets["Containers"], containerTemplates, locations);
                    // results
                    var locationZoneMappings = await locationZoneMappingsTask;
                    var unitsOfMeasure = await unitsOfMeasureTask;
                    var containerTemplateCompartments = await containerTemplateCompartmentsTask;
                    var containers = await containersTask;
                }
            }

            private int GetLastRow(ExcelWorksheet sheet)
            {
                int lastRow = 2; // EPPLUS counts the first row as index 1 to match Excel. We start on 2 since there's a header row.
                while (true)
                {
                    // Right now, the first column should always be required to ensure this is accurate
                    if (string.IsNullOrEmpty(sheet.Cells[lastRow, 1].GetValue<string>()))
                    {
                        return lastRow;
                    }

                    lastRow++;
                }
            }

            private async Task<IEnumerable<Locations>> ImportLocations(ExcelWorksheet sheet)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var locations = new List<Locations>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var locationName = sheet.Cells[row, 1].GetValue<string>();
                        if (_commands.debug) Console.WriteLine($"Location - Row {row} - {locationName}");
                        locations.Add(new Locations()
                        {
                            LocationName = sheet.Cells[row, 1].GetValue<string>(),
                            Barcode = sheet.Cells[row, 2].GetValue<string>(),
                            LocationType = sheet.Cells[row, 3].GetValue<string>(),
                            IsEnabled = sheet.Cells[row, 4].GetValue<string>() == "1",
                            Characteristics = sheet.Cells[row, 5].GetValue<string>(),
                            CreatedBy = CLI_CLIENT_NAME,
                            CreatedDate = DateTime.UtcNow,
                            LastUpdatedDate = DateTime.UtcNow,
                            LastUpdatedBy = CLI_CLIENT_NAME
                        });
                    }

                    await context.Locations.AddRangeAsync(locations);
                    await context.SaveChangesAsync();
                    return locations;
                }
            }

            private async Task<IEnumerable<LocationZones>> ImportLocationZones(ExcelWorksheet sheet)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var locationZones = new List<LocationZones>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var zoneName = sheet.Cells[row, 1].GetValue<string>();
                        if (_commands.debug) Console.WriteLine($"LocationZone - Row {row} - {zoneName}");
                        locationZones.Add(new LocationZones()
                        {
                            ZoneName = sheet.Cells[row, 1].GetValue<string>(),
                            InventoryTaskType = sheet.Cells[row, 2].GetValue<string>(),
                            CreatedBy = CLI_CLIENT_NAME,
                            CreatedDate = DateTime.UtcNow,
                            LastUpdatedDate = DateTime.UtcNow,
                            LastUpdatedBy = CLI_CLIENT_NAME
                        });
                    }

                    await context.LocationZones.AddRangeAsync(locationZones);
                    await context.SaveChangesAsync();
                    return locationZones;
                }
            }

            private async Task<IEnumerable<LocationZoneMappings>> ImportLocationZoneMappings(ExcelWorksheet sheet, IEnumerable<Locations> locations, IEnumerable<LocationZones> locationZones)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var locationZoneMappings = new List<LocationZoneMappings>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var location = locations.Single(loc => loc.LocationName == sheet.Cells[row, 1].GetValue<string>());
                        var locationZone = locationZones.Single(lz => lz.ZoneName == sheet.Cells[row, 2].GetValue<string>());
                        if (_commands.debug) Console.WriteLine($"LocationZoneMapping - Row {row} - ({location.LocationName} -> {locationZone.ZoneName})");
                        locationZoneMappings.Add(new LocationZoneMappings()
                        {
                            LocationPrimaryKey = location.PrimaryKey,
                            LocationZonePrimaryKey = locationZone.PrimaryKey
                        });
                    }

                    await context.LocationZoneMappings.AddRangeAsync(locationZoneMappings);
                    await context.SaveChangesAsync();
                    return locationZoneMappings;
                }
            }

            private async Task<IEnumerable<Products>> ImportProducts(ExcelWorksheet sheet)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var products = new List<Products>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var productName = sheet.Cells[row, 1].GetValue<string>();
                        if (_commands.debug) Console.WriteLine($"Product - Row {row} - {productName}");
                        products.Add(new Products()
                        {
                            Name = productName,
                            Sku = sheet.Cells[row, 2].GetValue<string>(),
                            PathToImage = sheet.Cells[row, 3].GetValue<string>(),
                            Characteristics = sheet.Cells[row, 4].GetValue<string>(),
                            CreatedBy = CLI_CLIENT_NAME,
                            CreatedDate = DateTime.UtcNow,
                            LastUpdatedDate = DateTime.UtcNow,
                            LastUpdatedBy = CLI_CLIENT_NAME
                        });
                    }

                    await context.Products.AddRangeAsync(products);
                    await context.SaveChangesAsync();
                    return products;
                }
            }

            private async Task<IEnumerable<UnitsOfMeasure>> ImportUnitsOfMeasure(ExcelWorksheet sheet, IEnumerable<Products> products)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var unitsOfMeasure = new List<UnitsOfMeasure>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var unitName = sheet.Cells[row, 1].GetValue<string>();
                        var productSku = sheet.Cells[row, 2].GetValue<string>();
                        var product = products.Single(p => p.Sku == productSku);
                        if (_commands.debug) Console.WriteLine($"UnitOfMeasure - Row {row} - {productSku}: {unitName}");
                        unitsOfMeasure.Add(new UnitsOfMeasure()
                        {
                            UnitName = unitName,
                            ProductPrimaryKey = product.PrimaryKey,
                            Weight = decimal.Parse(sheet.Cells[row, 3].GetValue<string>()),
                            IsBaseUnit = sheet.Cells[row, 4].GetValue<string>() == "1",
                            UpcBarcode = sheet.Cells[row, 5].GetValue<string>(),
                            UnitType = sheet.Cells[row, 6].GetValue<string>(),
                            BaseMultiple = int.Parse(sheet.Cells[row, 7].GetValue<string>()),
                            BaseUnitName = sheet.Cells[row, 8].GetValue<string>(),
                            CreatedBy = CLI_CLIENT_NAME,
                            CreatedDate = DateTime.UtcNow,
                            LastUpdatedDate = DateTime.UtcNow,
                            LastUpdatedBy = CLI_CLIENT_NAME
                        });
                    }

                    await context.UnitsOfMeasure.AddRangeAsync(unitsOfMeasure);
                    await context.SaveChangesAsync();
                    return unitsOfMeasure;
                }
            }

            private async Task<IEnumerable<ContainerTemplates>> ImportContainerTemplates(ExcelWorksheet sheet)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var contianerTemplates = new List<ContainerTemplates>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var templateName = sheet.Cells[row, 1].GetValue<string>();
                        if (_commands.debug) Console.WriteLine($"ContainerTemplate - Row {row} - {templateName}");
                        contianerTemplates.Add(new ContainerTemplates()
                        {
                            TemplateName = templateName,
                            TemplateDescription = sheet.Cells[row, 2].GetValue<string>(),
                            Height = int.Parse(sheet.Cells[row, 3].GetValue<string>()),
                            Length = int.Parse(sheet.Cells[row, 4].GetValue<string>()),
                            Width = int.Parse(sheet.Cells[row, 5].GetValue<string>()),
                            MaximumWeight = int.Parse(sheet.Cells[row, 6].GetValue<string>()),
                            IsGoodsToPerson = sheet.Cells[row, 7].GetValue<string>() == "1",
                            CreatedBy = CLI_CLIENT_NAME,
                            CreatedDate = DateTime.UtcNow,
                            LastUpdatedDate = DateTime.UtcNow,
                            LastUpdatedBy = CLI_CLIENT_NAME
                        });
                    }

                    await context.ContainerTemplates.AddRangeAsync(contianerTemplates);
                    await context.SaveChangesAsync();
                    return contianerTemplates;
                }
            }

            private async Task<IEnumerable<ContainerTemplateCompartments>> ImportContainerTemplateCompartments(ExcelWorksheet sheet, IEnumerable<ContainerTemplates> containerTemplates)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var ContainerTemplateCompartments = new List<ContainerTemplateCompartments>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var templateName = sheet.Cells[row, 1].GetValue<string>();
                        var containerSide = int.Parse(sheet.Cells[row, 2].GetValue<string>());
                        var xPosition = int.Parse(sheet.Cells[row, 3].GetValue<string>());
                        var yPosition = int.Parse(sheet.Cells[row, 4].GetValue<string>());
                        var template = containerTemplates.Single(ct => ct.TemplateName == templateName);
                        if (_commands.debug) Console.WriteLine($"ContainerTemplateCompartment - Row {row} - {templateName}: {containerSide} ({xPosition}, {yPosition})");
                        ContainerTemplateCompartments.Add(new ContainerTemplateCompartments()
                        {
                            TemplatePrimaryKey = template.PrimaryKey,
                            ContainerSide = containerSide,
                            Xposition = xPosition,
                            Yposition = yPosition,
                            HeightPercentage = int.Parse(sheet.Cells[row, 5].GetValue<string>()),
                            WidthPercentage = int.Parse(sheet.Cells[row, 6].GetValue<string>()),
                        });
                    }

                    await context.ContainerTemplateCompartments.AddRangeAsync(ContainerTemplateCompartments);
                    await context.SaveChangesAsync();
                    return ContainerTemplateCompartments;
                }
            }

            private async Task<IEnumerable<Containers>> ImportContainers(ExcelWorksheet sheet, IEnumerable<ContainerTemplates> containerTemplates, IEnumerable<Locations> locations)
            {
                using (var context = new SVTMVPContext(connectionString))
                {
                    int lastRow = GetLastRow(sheet);

                    var containers = new List<Containers>();
                    for (int row = 2; row < lastRow; row++)
                    {
                        var containerTemplateName = sheet.Cells[row, 1].GetValue<string>();
                        var barcode = sheet.Cells[row, 3].GetValue<string>();
                        var locationName = sheet.Cells[row, 4].GetValue<string>();
                        var containerTemplate = containerTemplates.Single(ct => ct.TemplateName == containerTemplateName);
                        var location = locations.Single(l => l.LocationName == locationName);
                        if (_commands.debug) Console.WriteLine($"Container - Row {row} - {containerTemplate.TemplateName} at {location.LocationName} with barcode {barcode}");
                        containers.Add(new Containers()
                        {
                            TemplatePrimaryKey = containerTemplate.PrimaryKey,
                            ContainerType = sheet.Cells[row, 2].GetValue<string>(),
                            Barcode = barcode,
                            LocationPrimaryKey = location.PrimaryKey,
                            Status = sheet.Cells[row, 5].GetValue<string>(),
                            Orientation = int.Parse(sheet.Cells[row, 6].GetValue<string>()),
                            PresentedSide = int.Parse(sheet.Cells[row, 7].GetValue<string>()),
                            Characteristics = sheet.Cells[row, 8].GetValue<string>(),
                            HasException = false,
                            CreatedBy = CLI_CLIENT_NAME,
                            CreatedDate = DateTime.UtcNow,
                            LastUpdatedDate = DateTime.UtcNow,
                            LastUpdatedBy = CLI_CLIENT_NAME
                        });
                    }

                    await context.Containers.AddRangeAsync(containers);
                    await context.SaveChangesAsync();
                    return containers;
                }
            }

        }
    }
}