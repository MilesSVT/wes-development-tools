using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task Product(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddProductAsync(verbArgs);
                    return;
                case nameof(VerbEnum.generate):
                    await GenerateProductsAsync(verbArgs);
                    return;
            }
        }

        internal async Task GenerateProductsAsync(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: product generate [NumberOfProducts]");
                return;
            }

            var numberOfProducts = int.Parse(args[0]);
            var maxId = context.Products.OrderByDescending(p => p.PrimaryKey).FirstOrDefault().PrimaryKey;

            var products = new List<Products>();
            for (int i = 1; i <= numberOfProducts; i++)
            {
                products.Add(ProductCreate($"P{maxId + i}"));
            }

            await context.Products.AddRangeAsync(products);
        }

        internal async Task AddProductAsync(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: product add [Name]");
                return;
            }

            await context.Products.AddAsync(ProductCreate(args[0]));
        }

        private Products ProductCreate(string name, string taskType = "Pick")
        {
            return new Products()
            {
                Name = name,
                Sku = $"{name}SKU",
                PathToImage = $"{name}.png",
                ProductConfirmations = new List<ProductConfirmations>()
                {
                    new ProductConfirmations()
                    {
                        TaskType = taskType,
                        CreatedBy = CLI_CLIENT_NAME,
                        CreatedDate = DateTime.UtcNow,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = CLI_CLIENT_NAME,
                    }
                },
                UnitsOfMeasure = new List<UnitsOfMeasure>()
                {
                    new UnitsOfMeasure()
                    {
                        UnitName = "Each",
                        Weight = 0.500M,
                        IsBaseUnit = true,
                        UnitType = "Each",
                        BaseMultiple = 1,
                        BaseUnitName = "Each",
                        CreatedBy = CLI_CLIENT_NAME,
                        CreatedDate = DateTime.UtcNow,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = CLI_CLIENT_NAME
                    },
                    new UnitsOfMeasure()
                    {
                        UnitName = "Carton",
                        Weight = 2.500M,
                        IsBaseUnit = false,
                        UnitType = "Carton",
                        BaseMultiple = 5,
                        BaseUnitName = "Each",
                        CreatedBy = CLI_CLIENT_NAME,
                        CreatedDate = DateTime.UtcNow,
                        LastUpdatedDate = DateTime.UtcNow,
                        LastUpdatedBy = CLI_CLIENT_NAME
                    }
                },
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            };
        }
    }
}
