﻿using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task Location(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddLocationAsync(verbArgs);
                    return;
            }
        }

        internal async Task AddLocationAsync(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: location add [LocationName] [Barcode] [LocationType] [IsEnabled]");
                return;
            }

            await context.Locations.AddAsync(new Locations()
            {
                LocationName = args[0],
                Barcode = args[1],
                LocationType = args[2],
                IsEnabled = args[3] == "1",
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            });
        }
    }
}
