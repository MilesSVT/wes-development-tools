using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task Empty(string[] _)
        {
            await context.Database.ExecuteSqlCommandAsync(@"
                delete from inventoryhistory; DBCC CHECKIDENT('inventoryhistory', RESEED, 0);
                delete from fulfillmentordercontainers;
                delete from containertemplatecompartments; DBCC CHECKIDENT('containertemplatecompartments', RESEED, 0);
                delete from unitsofmeasure; DBCC CHECKIDENT('unitsofmeasure', RESEED, 0);
                delete from products; DBCC CHECKIDENT('products', RESEED, 0);
                delete from nlog; DBCC CHECKIDENT('Nlog', RESEED, 0);
                delete from ArtifactErrors; DBCC CHECKIDENT('ArtifactErrors', RESEED, 0);
                delete from reportedmetrics; DBCC CHECKIDENT('reportedmetrics', RESEED, 0);
                delete from MessageLogRecords; DBCC CHECKIDENT('MessageLogRecords', RESEED, 0);
                delete from transportorderstatushistory; DBCC CHECKIDENT('transportorderstatushistory', RESEED, 0);
                delete from transportorders; DBCC CHECKIDENT('transportorders', RESEED, 0);
                delete from inventorytaskactions;
                delete from InventoryTaskErrors;DBCC CHECKIDENT('InventoryTaskErrors', RESEED, 0);
                delete from cyclecountfoureyes;DBCC CHECKIDENT('cyclecountfoureyes', RESEED, 0);
                delete from inventorytasks_cyclecount; 
                delete from inventorytasks_putaway;
                delete from PutAwayContainerLineItems;DBCC CHECKIDENT('PutAwayContainerLineItems', RESEED, 0);
                delete from PutAwayContainers;DBCC CHECKIDENT('PutAwayContainers', RESEED, 0);
                delete from inventorytasks_picks; 
                delete from InventoryTaskStatusHistory; DBCC CHECKIDENT('InventoryTaskStatusHistory', RESEED, 0);
                delete from inventorytasks; DBCC CHECKIDENT('inventorytasks', RESEED, 0);
                delete from LocationWorkModes;
                delete from FulfillmentOrderStatusHistory; DBCC CHECKIDENT('FulfillmentOrderStatusHistory', RESEED, 0);
                delete from FulfillmentOrderErrors; DBCC CHECKIDENT('FulfillmentOrderErrors', RESEED, 0);
                delete from FulfillmentOrderLines; DBCC CHECKIDENT('FulfillmentOrderLines', RESEED, 0);
                delete from FulfillmentOrders; DBCC CHECKIDENT('FulfillmentOrders', RESEED, 0);
                delete from inventory; DBCC CHECKIDENT('inventory', RESEED, 0);
                delete from ContainerExceptions; DBCC CHECKIDENT('ContainerExceptions', RESEED, 0);
                delete from ContainerCompartmentBarcodes; DBCC CHECKIDENT('ContainerCompartmentBarcodes', RESEED, 0);
                delete from containers; DBCC CHECKIDENT('Containers', RESEED, 0);
                delete from containertemplates; DBCC CHECKIDENT('containertemplates', RESEED, 0);
                delete from softbotparameters; DBCC CHECKIDENT('softbotparameters', RESEED, 0);
                delete from softbotservices; DBCC CHECKIDENT('softbotservices', RESEED, 0);
                delete from softbotregistrations; DBCC CHECKIDENT('softbotregistrations', RESEED, 0);
                delete from putdisplaycommands; DBCC CHECKIDENT('putdisplaycommands', RESEED, 0);
                delete from productconfirmations; DBCC CHECKIDENT('productconfirmations', RESEED, 0);
                delete from locationzonemappings;
                delete from locationzones; DBCC CHECKIDENT('locationzones', RESEED, 0);
                delete from externallocationmappings; DBCC CHECKIDENT('externallocationmappings', RESEED, 0);
                delete from locations; DBCC CHECKIDENT('locations', RESEED, 0);
            ");
        }
    }
}
