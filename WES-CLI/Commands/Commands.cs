﻿using System;
using SVT.MVP.DataAccess.EntityFramework;

namespace WESCLI
{
    public partial class Commands
    {
        public const string CLI_CLIENT_NAME = "CLI"; // Name to use for CreatedBy and UpdatedBy fields

        enum VerbEnum
        {
            add,
            generate
        }

        private readonly SVTMVPContext context;
        internal readonly Excel excel;
        public readonly bool debug;
        internal readonly Random random = new Random();

        public Commands(SVTMVPContext context, bool debug = true)
        {
            this.context = context;
            this.debug = debug;
            excel = new Excel(this);
        }
    }
}