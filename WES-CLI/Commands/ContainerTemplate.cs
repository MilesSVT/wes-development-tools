using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace WESCLI
{
    public partial class Commands
    {

        public async Task ContainerTemplate(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddContainerTemplateAsync(verbArgs);
                    return;
            }
        }

        internal async Task AddContainerTemplateAsync(string[] args)
        {

            if (args.Length != 7)
            {
                Console.WriteLine("Usage: containertemplate add [TemplateName] [TemplateDescription] [Height] [Length] [Width] [MaximumWeight] [IsGoodsToPerson]");
                return;
            }

            await context.ContainerTemplates.AddAsync(new ContainerTemplates()
            {
                TemplateName = args[0],
                TemplateDescription = args[1],
                Height = int.Parse(args[2]),
                Length = int.Parse(args[3]),
                Width = int.Parse(args[4]),
                MaximumWeight = int.Parse(args[5]),
                IsGoodsToPerson = args[6] == "1",
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            });
        }
    }
}