using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace WESCLI
{
    public partial class Commands
    {
        public async Task Container(string[] args)
        {
            var verb = args[0];
            var verbArgs = args.Skip(1).ToArray();
            switch (verb)
            {
                case nameof(VerbEnum.add):
                    await AddContainerAsync(verbArgs);
                    return;
                case nameof(VerbEnum.generate):
                    await GenerateContainersAsync(verbArgs);
                    return;
            }
        }

        internal async Task GenerateContainersAsync(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: container add [NumberOfContainers]");
                return;
            }

            var numberOfContainers = int.Parse(args[0]);

            var containers = new List<Containers>();
            var maxId = context.Containers.OrderByDescending(p => p.PrimaryKey).FirstOrDefault().PrimaryKey;

            for (int i = 1; i <= numberOfContainers; i++)
            {
                var template = context.ContainerTemplates.OrderBy(o => Guid.NewGuid()).ToList().First(); // random container template
                var locationPK = context.Locations.OrderBy(o => Guid.NewGuid()).ToList().First().PrimaryKey; // random location
                containers.Add(new Containers()
                {
                    TemplatePrimaryKey = template.PrimaryKey,
                    ContainerType = template.TemplateName,
                    Barcode = $"C{maxId + i}",
                    LocationPrimaryKey = locationPK,
                    Status = "Active",
                    Orientation = random.NextDouble() > 0.5 ? 180 : 0, // statistically half are gonna be rotated
                    PresentedSide = random.Next(0, 3), // values [0,2) are in the seed data so I'm leaving it like this
                    CreatedBy = CLI_CLIENT_NAME,
                    CreatedDate = DateTime.UtcNow,
                    LastUpdatedDate = DateTime.UtcNow,
                    LastUpdatedBy = CLI_CLIENT_NAME
                });
            }

            await context.Containers.AddRangeAsync(containers);
        }

        internal async Task AddContainerAsync(string[] args)
        {
            if (args.Length != 7)
            {
                Console.WriteLine("Usage: container add [TemplatePK] [ContainerType] [Barcode] [LocationPK] [Status] [Orientation] [PresentedSide]");
                return;
            }

            await context.Containers.AddAsync(new Containers()
            {
                TemplatePrimaryKey = int.Parse(args[0]),
                ContainerType = args[1],
                Barcode = args[2],
                LocationPrimaryKey = int.Parse(args[3]),
                Status = args[4],
                Orientation = int.Parse(args[5]),
                PresentedSide = int.Parse(args[6]),
                CreatedBy = CLI_CLIENT_NAME,
                CreatedDate = DateTime.UtcNow,
                LastUpdatedDate = DateTime.UtcNow,
                LastUpdatedBy = CLI_CLIENT_NAME
            });
        }
    }
}
