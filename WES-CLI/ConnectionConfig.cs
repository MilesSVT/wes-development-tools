namespace WESCLI
{
    public class ConnectionConfig
    {
        public string host { get; set; }
        public string database { get; set; }
        public string login { get; set; }
        public string password { get; set; }
    }
}