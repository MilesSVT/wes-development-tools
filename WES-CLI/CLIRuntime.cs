﻿using Newtonsoft.Json;
using SVT.MVP.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WESCLI
{
    public class CLIRuntime
    {
        private readonly SVTMVPContext context;
        private readonly Commands commands;

        private readonly Dictionary<string, Func<string[], Task>> commandMap;

        public CLIRuntime(string[] args)
        {
            // see if we're in debug
            var debug = false;
            if (args.Length > 0 && args[0] == "-d")
            {
                debug = true;
                args = args.Skip(1).ToArray();
            }

            try
            {
                // load db connection config from json
                ConnectionConfig connectionConfig = JsonConvert.DeserializeObject<ConnectionConfig>(File.ReadAllText(@"config.json"));
                var connectionString = $"Server={connectionConfig.host};Database={connectionConfig.database};User Id={connectionConfig.login};Password={connectionConfig.password};";

                context = new SVTMVPContext(connectionString); // TODO: refactor this and excel stuff since you can't use the same DbContext concurrently
                commands = new Commands(context, debug); // TODO: gotta be a better way...

                commandMap = new Dictionary<string, Func<string[], Task>>()
                {
                    { "help", commands.Help },
                    { "location", commands.Location },
                    { "container", commands.Container },
                    { "product", commands.Product },
                    { "order", commands.Order },
                    { "orderline", commands.OrderLine },
                    { "inventory", commands.Inventory },
                    { "reset", commands.Reset },
                    { "empty", commands.Empty },
                    { "import", commands.excel.Import }
                };

                HandleCommand(args).Wait();
                if (context.ChangeTracker.HasChanges())
                {
                    context.SaveChanges(); // saving here so we don't have to in every function
                }
            }
            catch (Exception error)
            {
                var tmpError = error;
                Console.WriteLine(error.Message);
                while (tmpError.InnerException != null)
                {
                    tmpError = tmpError.InnerException;
                }
                Console.WriteLine($"{tmpError.GetType().ToString()}: {tmpError.Message}");
                if (debug) Console.WriteLine(tmpError.StackTrace);
            }
            finally
            {
                context?.Dispose();
            }
        }

        private async Task HandleCommand(string[] args)
        {
            var command = args.Length == 0 ? "help" : args[0].ToLower(); // default to help command
            var commandArgs = args.Skip(1).ToArray(); // skip command to get command arguments
            // parse command and find matching function
            if (commandMap.TryGetValue(command, out Func<string[], Task> func))
            {
                await func(commandArgs);
                return;
            };
            Console.WriteLine($"Unable to find command \"{command}\"");
        }
    }
}